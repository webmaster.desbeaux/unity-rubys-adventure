﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RubyController : MonoBehaviour
{
    public float speed = 3.0f;
    Rigidbody2D rb;
    float horizontal;
    float vertical;

    public int maxHealth = 5;
    public int health { get { return currentHealth; } }
    int currentHealth;

    bool isInvincible;
    float invincibleTimer;
    public float timeInvincible = 2.0f;

    Animator animator;
    Vector2 lookDirection = new Vector2(1, 0);

    public GameObject projectilePrefab;
    public ParticleSystem hitParticule;

    AudioSource audioSource;
    public AudioClip audioHit;
    public AudioClip audioProjectile;

    /// <summary>
    /// Fonction au démarrage
    /// </summary>
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        currentHealth = maxHealth;
        animator = GetComponent<Animator>();

        audioSource = GetComponent<AudioSource>();
    }

    /// <summary>
    /// Jouer un son une seule fois
    /// </summary>
    /// <param name="audioClip">Audio clip  ajouer une seul fois</param>
    public void PlaySound(AudioClip audioClip)
    {
        audioSource.PlayOneShot(audioClip);
    }

    /// <summary>
    /// Fonction à chaque frame
    /// </summary>
    void Update()
    {
        //Keep this because fixedUpdate is not execute all the time : possibly miss an input
        horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");

        Vector2 move = new Vector2(horizontal, vertical);

        if (!Mathf.Approximately(move.x, 0) || !Mathf.Approximately(move.y, 0))
        {
            lookDirection.Set(move.x, move.y);
            lookDirection.Normalize();
        }

        animator.SetFloat("Look X", lookDirection.x);
        animator.SetFloat("Look Y", lookDirection.y);
        animator.SetFloat("Speed", move.magnitude);

        if (isInvincible)
        {
            invincibleTimer -= Time.deltaTime;
            if (invincibleTimer < 0)
                isInvincible = false;
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            Launch();
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            RaycastHit2D hit = Physics2D.Raycast(rb.position + Vector2.up * 0.2f, lookDirection, 1.5f, LayerMask.GetMask("NPC"));
            if (hit.collider != null)
            {
                //Debug.Log("RayCast collision with " + hit.collider.gameObject);

                NonPlayerCharacter jambi = hit.collider.GetComponent<NonPlayerCharacter>();
                if (jambi != null)
                    jambi.DisplayDialog();
            }
        }
    }

    /// <summary>
    /// Modify physic element in this function
    /// </summary>
    void FixedUpdate()
    {
        Vector2 position = rb.position;
        position.x = position.x + speed * horizontal * Time.deltaTime;
        position.y = position.y + speed * vertical * Time.deltaTime;

        rb.MovePosition(position);
    }

    /// <summary>
    /// Changer la vie du joueur
    /// </summary>
    /// <param name="amount">montant a ajouter ou retirer à la vie du joueur</param>
    public void ChangeHealth(int amount)
    {
        if (amount < 0)
        {
            if (isInvincible)
                return;

            invincibleTimer = timeInvincible;
            isInvincible = true;

            animator.SetTrigger("Hit");
            Instantiate<ParticleSystem>(hitParticule, transform.position + new Vector3() * 0.5f, Quaternion.identity);

            PlaySound(audioHit);

        }

        currentHealth = Mathf.Clamp(currentHealth + amount, 0, maxHealth);
        UIHealthBar.instance.SetValue(currentHealth / (float)maxHealth);
    }

    /// <summary>
    /// Lancer un objet
    /// </summary>
    void Launch()
    {
        GameObject projectileObject = Instantiate(projectilePrefab, rb.position + (Vector2.up * 0.5f), Quaternion.identity);
        Projectile projectile = projectileObject.GetComponent<Projectile>();
        projectile.Launch(lookDirection, 300);

        animator.SetTrigger("Launch");

        PlaySound(audioProjectile);
    }
}
