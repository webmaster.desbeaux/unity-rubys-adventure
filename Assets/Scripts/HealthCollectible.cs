﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthCollectible : MonoBehaviour
{
    public ParticleSystem collectibleParticule;
    public AudioClip collectedClip;

    void OnTriggerEnter2D(Collider2D collision)
    {
        RubyController controller = collision.GetComponent<RubyController>();

        if (controller != null)
        {
            if (controller.health < controller.maxHealth)
            {
                
                controller.ChangeHealth(1);
                Destroy(gameObject);
                Instantiate(collectibleParticule, transform.position + (Vector3.up * 0.5f), Quaternion.identity);
                controller.PlaySound(collectedClip);
            }
        }
    }
}
